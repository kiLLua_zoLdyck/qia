package com.scc.androidmysql;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

public class About extends MainActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.about, frameLayout);
	}
	
	// when back button is pressed
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) 
		{
		    if (keyCode == KeyEvent.KEYCODE_BACK) 
		    {
		        Intent back = new Intent (getBaseContext(), HomeActivity.class);
		        startActivity(back);
		    }

		    return false;
		}
}
