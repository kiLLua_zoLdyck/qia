package com.scc.androidmysql;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class ViewGrades extends ListActivity {
	
	
	
	// Progress Dialog
	private ProgressDialog pDialog;
	
	// testing on Emulator:
	private static final String VIEW_GRADES_URL = "http://10.0.2.2:80/QiA/ViewGrades.php";
			
	// JSON IDS:
//	private static final String TAG_SUCCESS = "Success";
	private static final String TAG_POSTS = "posts";
//	private static final String TAG_POST_ID = "post_id";
	private static final String TAG_SEMESTER = "cSemester";
	private static final String TAG_SCHOOLYEAR = "cSchoolYear";
	private static final String TAG_SUBJECTCODE = "cSubjectCode";
	private static final String TAG_DESCRIPTION = "cDescription";
	private static final String TAG_FINALGRADE = "nFGrade";
	private static final String TAG_COMPLETIONGRADE = "nCGrade";
	private static final String TAG_CREDIT = "nCredit";
	
	private JSONArray vGrades = null;
	private ArrayList<HashMap<String, String>> ViewGrades;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_grades);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		new LoadComments().execute();
	}
	
	public void updateJSONdata() {
		ViewGrades = new ArrayList<HashMap<String, String>>();
		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(VIEW_GRADES_URL);
		
		try {
			
		vGrades = json.getJSONArray(TAG_POSTS);
		for (int i = 0; i < vGrades.length(); i++) {
		JSONObject c = vGrades.getJSONObject(i);
		
		String cSemester = c.getString(TAG_SEMESTER);
		String cSchoolYear = c.getString(TAG_SCHOOLYEAR);
		String cSubjectCode = c.getString(TAG_SUBJECTCODE);
		String cDescription = c.getString(TAG_DESCRIPTION);
		String nFGrade = c.getString(TAG_FINALGRADE);
		String nCGrade = c.getString(TAG_SUBJECTCODE);
		String nCredit = c.getString(TAG_CREDIT);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(TAG_SEMESTER, cSemester);
		map.put(TAG_SCHOOLYEAR, cSchoolYear);
		map.put(TAG_SUBJECTCODE, cSubjectCode);
		map.put(TAG_DESCRIPTION, cDescription);
		map.put(TAG_FINALGRADE, nFGrade);
		map.put(TAG_COMPLETIONGRADE, nCGrade);
		map.put(TAG_CREDIT, nCredit);
		ViewGrades.add(map);
		}
		} catch (JSONException e) {
			e.printStackTrace();
			}
	}
	private void updateList() {
	ListAdapter adapter = new SimpleAdapter(this, ViewGrades,
	R.layout.single_post, new String[] { TAG_SEMESTER,
	TAG_SCHOOLYEAR, TAG_SUBJECTCODE, TAG_DESCRIPTION, 
	TAG_FINALGRADE, TAG_COMPLETIONGRADE, TAG_CREDIT}, 
	new int[] { R.id.semester,R.id.school_year, R.id.subject_code,
	R.id.description,R.id.fGrade,R.id.cGrade,R.id.credit });
	setListAdapter(adapter);
	ListView lv = getListView();
	lv.setOnItemClickListener(new OnItemClickListener() {
	@Override
	public void onItemClick(AdapterView<?> parent, View view,
	int position, long id) {
	}
	});
	}
	
	public class LoadComments extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ViewGrades.this);
			pDialog.setMessage("GRADES...");
			pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			pDialog.show();
		}
		
		@Override
		protected Boolean doInBackground(Void... arg0) {
			updateJSONdata();
			return null;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			updateList();
		}
		}
		}