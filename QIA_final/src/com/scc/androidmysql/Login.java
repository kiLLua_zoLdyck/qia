package com.scc.androidmysql;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;

	
	public class Login extends Activity implements OnClickListener {
	private EditText idnum, answer;
	private ImageView mSubmit;
	private TextView question;
    Random r = new Random();
    int i1 = (r.nextInt(4));

	// Progress Dialog
	private ProgressDialog pDialog;

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	// testing on Emulator:
	private static final String LOGIN_URL = "http://10.0.2.2:80/QiA/login.php";
	private static final String TAG_SUCCESS = "Success";
	private static final String TAG_MESSAGE = "message";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		// setup input fields
		idnum = (EditText) findViewById(R.id.id_num);
		answer = (EditText) findViewById(R.id.answer);
		question = (TextView) findViewById(R.id.question);
		
        
        switch(i1){
        case 1: question.setText("Middle Name:");break;
        case 2: question.setText("Course:");break;
        case 3: question.setText("Elementary Year Graduated:");break;
        case 4: question.setText("HighSchool Year Graduated:");break;
        default:
        	Toast.makeText(getBaseContext(), "Please Wait . . .", Toast.LENGTH_LONG).show();
        	Intent i = new Intent(getBaseContext(), Login.class);
			finish();
			startActivity(i);
        	break;
        }

		
		// setup buttons
		mSubmit = (ImageView) findViewById(R.id.btn_login);

		// register listeners
		mSubmit.setOnClickListener(this);
		
		}
		
	
	@Override
	public void onClick(View v) {
	// TODO Auto-generated method stub
		String Userid = idnum.getText().toString();
		String Useranswer = answer.getText().toString();
		if(Userid.equals("")|| Userid == null)
		{
			Toast.makeText(getBaseContext(), "please enter your ID Number!", Toast.LENGTH_LONG).show();
		}
		
		else if(Useranswer.equals("") || Useranswer == null){
			Toast.makeText(getBaseContext(), "please enter your Answer!", Toast.LENGTH_LONG).show();
		}
		else{
			new AttemptLogin().execute();
		}
	
		} //close sa listener */
	
	//attempt login
	
	class AttemptLogin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(Login.this);
		pDialog.setMessage("Attempting login...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
		}	// on pre exec */
		
		@Override
		protected String doInBackground(String... args) {
		// TODO Auto-generated method stub
		// Check for success tag
		int Success;
		String Userid = idnum.getText().toString();
		String Useranswer = answer.getText().toString();
		String UserQuest = String.valueOf(i1);
		try {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("idnum", Userid));
		params.add(new BasicNameValuePair("answer", Useranswer));
		params.add(new BasicNameValuePair("question", UserQuest));
		Log.d("request!", "starting");
		
		// getting product details by making HTTP request
		JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",params);
		
		// check your log for json response
		Log.d("Login attempt", json.toString());
		// json success tag
		Success = json.getInt(TAG_SUCCESS);
		
		if (Success == 1) {
		Log.d("Login Successful!", json.toString());
		// save user data
		SharedPreferences sp = PreferenceManager
		.getDefaultSharedPreferences(Login.this);
		Editor edit = sp.edit();
		edit.putString("idnum", Userid);
		edit.commit();
		Intent i = new Intent(Login.this, HomeActivity.class);
		finish();
		startActivity(i);
		return json.getString(TAG_MESSAGE);
		} else {
		Log.d("Login Failure!", json.getString(TAG_MESSAGE));
		return json.getString(TAG_MESSAGE);
		}
		} catch (JSONException e) {
		e.printStackTrace();
		}
		return null;
		}   	// close for do in bgnd


		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();

			if (file_url != null) {
			Toast.makeText(Login.this, file_url, Toast.LENGTH_LONG).show();
			}
			} //on post exec */
			}
			}