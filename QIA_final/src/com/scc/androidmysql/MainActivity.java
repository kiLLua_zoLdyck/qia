package com.scc.androidmysql;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

public class MainActivity extends Activity {

   private String[] drawerListViewItems;
   private DrawerLayout drawerLayout;
   private ListView drawerListView;
   protected FrameLayout frameLayout;
   private ImageView main_content;
   private ActionBarDrawerToggle actionBarDrawerToggle;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_main); 
       
       
      // String drawerListViewItems []= {"About", "View Grades", "Credits", "Vision", "Mission", "Logout"};
       //ImageView drawerListViewIcons =  {R.drawable.ic_home, R.drawable.ic_view, R.drawable.ic_credits, R.drawable.ic_vision, R.drawable.ic_mission, R.drawable.ic_logout};
      
     
      // get list items from strings.xml
       drawerListViewItems = getResources().getStringArray(R.array.items);
       
       //get FrameLayout defined in activity_main.xml
       frameLayout = (FrameLayout)findViewById(R.id.content_frame);
       
       // get ListView defined in activity_main.xml
       drawerListView = (ListView) findViewById(R.id.left_drawer);

       // Set the adapter for the list view
       drawerListView.setAdapter(new ArrayAdapter<String>(this,
               R.layout.drawer_listview_item, drawerListViewItems));

       // 2. App Icon 
       drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
       

       // 2.1 create ActionBarDrawerToggle
       actionBarDrawerToggle = new ActionBarDrawerToggle(
               this,                  /* host Activity */
               drawerLayout,         /* DrawerLayout object */
               R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
               R.string.drawer_open,  /* "open drawer" description */
               R.string.drawer_close  /* "close drawer" description */
               );

       // 2.2 Set actionBarDrawerToggle as the DrawerListener
       drawerLayout.setDrawerListener(actionBarDrawerToggle);

       // 2.3 enable and show "up" arrow
       getActionBar().setDisplayHomeAsUpEnabled(true); 

       // just styling option
       //drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

       drawerListView.setOnItemClickListener(new DrawerItemClickListener());
   }

   @Override
   protected void onPostCreate(Bundle savedInstanceState) {
       super.onPostCreate(savedInstanceState);
       // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
   }

   @Override
   public void onConfigurationChanged(Configuration newConfig) {
       super.onConfigurationChanged(newConfig);
       actionBarDrawerToggle.onConfigurationChanged(newConfig);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {

        // call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns true
       // then it has handled the app icon touch event

       if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
           return true;
       }
       return super.onOptionsItemSelected(item);
     
   }

   private class DrawerItemClickListener implements ListView.OnItemClickListener {
       @Override
       public void onItemClick(AdapterView parent, View view, int position, long id) {
         //  Toast.makeText(MainActivity.this, ((TextView)view).getText(), Toast.LENGTH_LONG).show();
           //drawerLayout.closeDrawer(drawerListView);
    	     
           switch (position){
           
           case 0:
        	   
        	   Intent about = new Intent (getBaseContext(), About.class);
           	   startActivity(about);
        	   
        	   break;
        	   
           case 1:
        	   
        	   Intent view_g = new Intent (getBaseContext(), ViewGrades.class);
        	   startActivity(view_g);
        	  
        	   break;
       	   
          case 2:
        	  
        	  Intent credits = new Intent (getBaseContext(), Credits.class);
        	  startActivity(credits);
        	   break;
        	   
           case 3:
        	   
        	   Intent vision = new Intent (getBaseContext(), Vision.class);
        	   startActivity(vision);
        	   
        	   break;
        	  
           case 4:
        	   
        	   Intent mission = new Intent (getBaseContext(), Mission.class);
        	   startActivity(mission);
        	   
        	   break;
        	 
           case 5:
                		   
        	   AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        	     
        	   setTheme(R.style.myBackgroundStyle);
               // Setting Dialog Title
               alertDialog.setTitle("Confirm Logout");

               // Setting Dialog Message
               alertDialog.setMessage("Are you sure you want to logout?");

               // Setting Icon to Dialog
               alertDialog.setIcon(R.drawable.ic_logout);

               // Setting Positive "Yes" Button
               alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog,int which) {

               // code to invoke YES event
               //Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                	 
            	   Intent yes = new Intent (getBaseContext(), Login.class);
            	   finish();
            	   startActivity(yes);
                   }
               });

               // Setting Negative "NO" Button
               alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int which) {
                   // Write your code here to invoke NO event
                  // Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                   }
               });
               
               // Showing Alert Message
               alertDialog.show();
               
              	 break;
              	
              	 
        	   default:
        		   break;
        	 
           }
       
        // update selected item and title, then close the drawer
        			drawerListView.setItemChecked(position, true);
        			drawerListView.setSelection(position);
        			drawerLayout.closeDrawer(drawerListView);
       }
   }

public int AlertButton(int position) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public void onBackPressed() {
    finish();
}
}


